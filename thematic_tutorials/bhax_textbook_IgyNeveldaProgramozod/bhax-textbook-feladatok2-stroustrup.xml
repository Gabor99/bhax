<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Stroustrup!!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>                   
    <section>
        <title>JDK osztályok</title>
        <para>
            Írjunk olyan Boost C++ programot (indulj ki például a fénykardból) amely kilistázza a JDK összes osztályát (miután kicsomagoltuk az src.zip állományt, arra ráengedve)!
        </para>
        <para>
            A program forrása: <link xlink:href="https://gitlab.com/Gabor99/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/prog2/Prog2_Stroustrup/java_classes/java_classes.cpp">java_classes.cpp</link>
        </para>
        <para>
            Megjegyzés: az src.zip-ben az openjdk8-as verziót használtuk
        </para>
        <screen><![CDATA[
sudo apt install openjdk-8-source
cp /usr/lib/jvm/openjdk-8/src.zip ~
unzip src.zip
]]>
        </screen>
        <para>
            A forráskód:
        </para>
        <programlisting language="c++"><![CDATA[
#include <iostream>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/xpressive/regex_actions.hpp>

using namespace std;
int main(){
    vector<string> classes; 
    boost::filesystem::path targetDir("./src/");

    boost::filesystem::recursive_directory_iterator iter(targetDir), eod;

    BOOST_FOREACH(boost::filesystem::path const& i, make_pair(iter, eod))
    {
        if (is_regular_file(i) && i.filename().string().find(".java") != string::npos){
            cout << i.filename().string() << endl;
            classes.push_back(i.filename().string());
        }
    }
    cout << "===========================================" << endl;
    cout << classes.size() << " db java osztály van az src.zip-ben"<< endl;
}]]>
        </programlisting>
        <para>
            A program futtatása és eredménye:
        </para>
        <screen><![CDATA[
g++ java_classes.cpp -o java_classes -lboost_system -lboost_filesystem
./java_classes

...
Parser.java
ReaderUTF8.java
ParserSAX.java
XMLWriter.java
SAXParserImpl.java
SAXParser.java
PropertiesDefaultHandler.java
===========================================
17630 db java osztály van az src.zip-ben]]>
        </screen>
    </section>

    <section>
        <title>Másoló-mozgató szemantika</title>
        <para>
            Kódcsipeteken (copy és move ctor és assign) keresztül vesd össze a C++11 másoló és a mozgató szemantikáját, a mozgató konstruktort alapozd a mozgató értékadásra!
        </para>
    </section>

    <section>
        <title>Hibásan implementált RSA törése</title>
        <para>
            Készítsünk betű gyakoriság alapú törést egy hibásan implementált RSA kódoló:
            <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_3.pdf">https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_3.pdf</link> (71-73 fólia) által készített titkos szövegen.
        </para>
        <para>
            Az RSA algoritmus egy asszimetrikus kulcsú titkosítás.
        </para>
        <para>
            Az aszimmetrikus kulcsú titkosításnál a kódoláshoz és a dekódoláshoz nem ugyanazt a kulcsot használják, hanem a kódoló és dekódoló kulcsok egy kulcspárt alkotnak. A kulcspárt alkotó két kulcs szorosan összetartozik (egy kulcsnak pontosan csak egy párja létezik), ám ezeket egymásból kiszámítani nagyon nehéz. Ezt a különleges kapcsolatot a kulcspárt létrehozó algoritmus garantálja. A kulcspár egyik felével kódolt üzenetet kizárólag a kulcspár másik felének segítségével lehet dekódolni.
        </para>
        <para>
            Az RSA algoritmus alapvetően titkosításra szolgál. Azonban speciális tulajdonságából adódóan (a kódolás és dekódolás matematikailag ugyanaz a lépéssorozat, csak a másik kulccsal kell elvégezni) pontosan ugyanez az algoritmus alkalmazható digitális aláírásra is. A titkosító algoritmust ezen kívül kulcsegyeztetésre is szokták használni. Az RSA algoritmus az egész számok faktorizációs problémájára épül.
        </para>
        <para>
            <command>RSA algoritmus</command>
        </para>
        <para>
            Az RSA algoritmus eredendően titkosító algoritmus: rejtjelezett kommunikációt valósít meg két távoli fél között. Rendelkezik azonban egy nagyon előnyös tulajdonsággal: a kódolás és dekódolás sorrendje felcserélhető (vagyis a nyílt szöveget először „visszafejtve” majd utána kódolva szintén az eredeti szöveget kapjuk vissza). Ennek révén alkalmas a digitális aláírás megvalósítására is. Ekkor ugyanis ugyanabban a formában az aláíró fél saját titkos kulcsának alkalmazásával először „dekódolja” (vagyis aláírja) a dokumentumot, amelyet az ellenőrző fél az aláírás „kódolásával” tud ellenőrizni.
        </para>
        <para>
            <command>Az RSA biztonsága</command>
        </para>
        <para>
            Az RSA kulcsok egymásból való kiszámíthatatlansága az egész számok faktorizációs problémáján alapszik, azonban a kettő ekvivalenciája nem bizonyított. Az RSA algoritmus biztonságosságát az évek során több kutatócsoport vizsgálta. Számos támadási módszert találtak, amelyek bizonyos speciális, gyenge paraméterválasztás esetén működnek, azonban érdemi törést – amikor a nyilvános kulcsból a titkos kulcsot reális időn belül meg tudták volna határozni, vagy a titkos kulcs ismerete nélkül a kódolt üzeneteket vissza tudták volna fejteni – nem sikerült felfedezni. Az eddigi kutatási eredmények szerint napjainkban (2003) az 1024 bites modulusú RSA használatát tartják biztonságosnak.
        </para>

        <para>
            Az óra során implementált kódok:
        </para>
        <itemizedlist>
            <listitem>
                <para>
                    <link xlink:href="https://gitlab.com/Gabor99/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/prog2/Prog2_Stroustrup/RSA/Kulcspar.java">GitLab/prog2/stroustrup/RSA/Kulcsar.java</link>
                </para>
            </listitem>
            <listitem>
                <para>
                    <link xlink:href="https://gitlab.com/Gabor99/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/prog2/Prog2_Stroustrup/RSA/rsa.java">GitLab/prog2/stroustrup/RSA/rsa.java</link>
                </para>
            </listitem>
            <listitem>
                <para>
                    <link xlink:href="https://gitlab.com/Gabor99/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/prog2/Prog2_Stroustrup/RSA/RSApelda.java">prog2/stroustrup/RSA/RSApelda.java</link>
                </para>
            </listitem>
        </itemizedlist>
        <programlisting language="java"><![CDATA[
public class RSAPelda {
    public static void main(String[] args){
        KulcsPar jSzereplo = new KulcsPar();

        // i -> j
        String tisztaSzoveg = "Hello, Vilag!";

        // kódol i
        byte[] buffer = tisztaSzoveg.getBytes();
        java.math.BigInteger[] titkos = new java.math.BigInteger[buffer.length];

        for (int i = 0; i < titkos.length; i++) {
            titkos[i] = new java.math.BigInteger(new byte[] {buffer[i]});
            titkos[i] = titkos[i].modPow(jSzereplo.e, jSzereplo.m);
        }

        // ------------------------------------------------------------------

        //dekódol j
        for (int i= 0; i<titkos.length; ++i){
            titkos[i] = titkos[i].modPow(jSzereplo.d, jSzereplo.m);
            buffer[i] = titkos[i].byteValue();
        }

        System.out.println(new String(buffer));
    }
}]]>
        </programlisting>
    </section>


    <section>
        <title>Változó argumentumszámú ctor</title>
        <para>
            Készítsünk olyan példát, amely egy képet tesz az alábbi projekt Perceptron osztályának bemenetére és a Perceptron ne egy értéket, hanem egy ugyanakkora méretű „képet” adjon vissza. (Lásd még a 4 hét/Perceptron osztály feladatot is.)
        </para>
    </section>

    <section>
        <title>Összefoglaló</title>
        <para>
            Az előző 4 feladat egyikéről írj egy 1 oldalas bemutató „esszé” szöveget!
        </para>
    </section>
    <section>
        <title>EPAM: It's gone. Or is it?</title>
        <para>
            Adott a következő osztály:
        </para>
        <para>
            Alap source: <link xlink:href="https://gitlab.com/Gabor99/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/prog2/Prog2_Stroustrup/its_gone_or_is_it/WriterExample.java">WriterExample.java</link>
        </para>
        <para>
            Fixed source: <link xlink:href="https://gitlab.com/Gabor99/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/prog2/Prog2_Stroustrup/its_gone_or_is_it/FixedWriterExample.java">FixedWriterExample.java</link>
        </para>
        <programlisting language="java"><![CDATA[
public class BugousStuffProducer {
    private final Writer writer;
    public BugousStuffProducer(String outputFileName) throws IOException {
        writer = new FileWriter(outputFileName);
    }
    public void writeStuff() throws IOException {
        writer.write("Stuff");
    }
    @Override
    public void finalize() throws IOException {
        writer.close();
    }
}]]>
        </programlisting>
        <para>
            Mutass példát arra az esetre, amikor előfordulhat, hogy bár a program futása során meghívtuk a <literal>writeStuff()</literal> metódust, a fájl, amibe írtunk még is üres.
        </para>
        <para>
            Magyarázd meg, miért. Mutass alternatívát.
        </para>
        <para>
            Példa a használatra, ahol előfordul a hiba:
        </para>
        <programlisting language="java"><![CDATA[
...
public static void main(String[] args) throws IOException {
    BugousStuffProducer stuffProducer = new BugousStuffProducer("someFile.txt");
    stuffProducer.writeStuff();
    System.gc();
}]]>
        </programlisting>
        <para>
            A hiba akkor jön elő, mikor az objektum lezárását (jelen esetben a FileWriter) a Garbage Colletor-ra bízzuk. Ez a hiba tipikusan azoknál az objektumoknál szokott előfordulni ahol stream-eket, socket-eket kezelünk (jobban összefoglalva a <code>Closable</code> vagy az <code>AutoClosable</code> interfészt leimplementáló objektumoknál). Ugye amikor a Garbage Collector lefut egy adott objektumra, akkor az objektum osztályában definiált <code>finalize()</code> metódus fog lefutni. Viszont ebben az esetben a Garbage Collector miatt itt nem jó taktika a stream lezárása, mivel nem mindig fog bezáródni a fájl, így üres fájlt hagyva nekünk eredményképpen.
        </para>
        <para>
            Egyszerű javítás a kódban:
        </para>
        <programlisting language="java"><![CDATA[
...
public static void main(String[] args) throws Exception {
    try (BugousStuffProducer stuffProducer = new BugousStuffProducer("test.txt")) {
        stuffProducer.writeStuff();
    }
}]]>
        </programlisting>
        <para>
            Ezt egyszerűen ki tudjuk javítani. Amelyik objektumba le lett implementálva a <code>Closable</code> vagy az <code>AutoClosable</code> interfész, ott az ezekkel végzett műveletet szimplán beleágyazzuk egy <code>try{}</code> részbe
        </para>
    </section>
    <section>
        <title>EPAM: Kind of equal</title>
        <para>
            
        </para>

        <programlisting language="java"><![CDATA[
// Given
String first = "...";
String second = "...";
String third = "...";
// When
var firstMatchesSecondWithEquals = first.equals(second);
var firstMatchesSecondWithEqualToOperator = first == second;
var firstMatchesThirdWithEquals = first.equals(third);
var firstMatchesThirdWithEqualToOperator = first == third;]]>
        </programlisting>
        <para>
            Változtasd meg a <literal>String third = "...";</literal> sort úgy, hogy a <literal>firstMatchesSecondWithEquals</literal>, <literal>firstMatchesSecondWithEqualToOperator</literal>, <literal>firstMatchesThirdWithEquals</literal> értéke <literal>true</literal>, a <literal>firstMatchesThirdWithEqualToOperator</literal> értéke pedig <literal>false</literal> legyen. Magyarázd meg, mi történik a háttérben.
        </para>
    </section>

    <section>
        <title>EPAM: Java GC</title>
        <para>
            Mutasd be nagy vonalakban hogyan működik Java-ban a GC (Garbage Collector). Lehetséges az OutOfMemoryError kezelése, ha igen milyen esetekben?
        </para>
        <para>Források:</para>
        <itemizedlist>
            <listitem>
                <para>
                    <link xlink:href="https://medium.com/@hasithalgamge/seven-types-of-java-garbage-collectors-6297a1418e82">https://medium.com/@hasithalgamge/seven-types-of-java-garbage-collectors-6297a1418e82
                    </link>
                </para>
            </listitem>
            <listitem>
                <para>
                    <link xlink:href="https://stackoverflow.com/questions/2679330/catching-java-lang-outofmemoryerror">
                        https://stackoverflow.com/questions/2679330/catching-java-lang-outofmemoryerror
                    </link>
                </para>
            </listitem>
        </itemizedlist>
        <para>A Java programozási nyelv tartalmazza az automatikus Garbage Colletion-t, de ezekből több féle változat van (Java 12-t nézve)</para>
        <orderedlist>
            <listitem>
                <para>
                    Serial Garbage Collector: egy szálas környezethez tervezték. Megállít minden alkalmazásszálat amikor fut, és ezután egy szálon történik meg a Garbage Collection
                </para>
                <para>
                    Használatához a következő argumentumot kell megadni: <command>java -XX:+UseSerialGC -jar Something.java</command>
                </para>
            </listitem>
            <listitem>
                <para>
                    Parallel Garbage Collector: több szálat használ, ugyan úgy megállítja az összes futó alkalmazásszálat és ezután több szálon hajtja végre a Garbage Collection-t. Azokhoz az alkalmazásokhoz lett tervezve, ahol a program kibír néhány megállást.
                </para>
                <para>
                    Használatához a következő argumentumot kell megadni: <command>java -XX:+UseParallelGC -jar Something.java</command>
                </para>
                <para>Meg tudjuk adni a GC-re használt szálak maximum számát (<command>-XX:ParallelGCThreads=&lt;N&gt;</command>), megállási idejét (<command>-XX:MaxGCPauseMillis=&lt;N&gt;</command>), "áteresztőképességét" (<command>-XX:GCTimeRatio=&lt;N&gt;</command>) és a heap méretét (<command>-Xmx&lt;N&gt;</command>)</para>
            </listitem>
            <listitem>
                <para>
                    CMS Garbage Collector (Concurrent Mark Sweep): Több GC szálat használ. Átszkenneli a heap memóriát, hogy megjelölje a azokat az objektumokat, amelyeket már nem használunk, ezután eltávolítja ezeket. Olyan alkalmazásokhoz terveztél, ahol kisebb megállási időre van szükségünk, de emellett a program futása közben a processzornak fel is kell dolgozni a Garbage Colletion folyamatát. Röviden több processzort használ, de ha erre lehetőségünk van, akkor jobb mint a parallel GC
                </para>
                <para>
                    Használatához a következő argumentumot kell megadni: <command>java -XX:+USeParNewGC -jar Something.java</command>
                </para>

            </listitem>
            <listitem>
                <para>
                    G1 Garbage Collector (Garbage First): Többprocesszoros eszközökre lett tervezve, amelyek nagy a memóriaterület rendelkeznek. A heap-et felosztja régiókra, és ezekben végi el a garbage colletion-t párhuzamosan. Nagyon hasonló a CMS GC-hez, de ennek a memóriahasználata jobb. Két fázisa van, amely a megjelölés (Marking) és a takarítás (Sweeping).
                </para>
                <para>
                    Használatához a következő argumentumot kell megadni: <command>java -XX:+UseG1GC -jar Something.java</command>
                </para>

            </listitem>
            <listitem>
                <para>
                    Epsilon Garbage Collector: ez egy passzív GC. Csak lefoglalja a memóriát az alkalmazásnak a futtatáskor, de nem gyűjti össze a nem használt objektumokat. Amikor az alkalmazás kifogy a heap-ből, akkor a JVM leáll. Azaz ez a GC engedi az alkalmazásokat, hogy a memóriából kifogyjanak és leálljanak (crash). Célja ennek a GC-nek, hogy leteszteljük az alkalmazásunknak a teljesítményét és stabilitását. Az aktív GC-k hatással vannak az alkalmazás teljesítményére, de mivel passzívat használunk így ez nem lesz hatással a programunkra. Egyetlen argumentuma van, az pedig azt kapcsolja be, hogy a heap dump-ot legenerálja, amikor a programunk crashel. (<command>XX:HeapDumpOnOutOfMemoryError</command>)
                </para>
                <para>
                    Használatához a következő argumentumot kell megadni: <command>java -XX:+UseEpsilonGC -jar Something.java</command>
                </para>

            </listitem>
            <listitem>
                <para>
                    Z garbage collector
                </para>
                <para>
                    Használatához a következő argumentumot kell megadni: <command>java -XX:+UseZGC -jar Something.java</command>
                </para>
            </listitem>
            <listitem>
                <para>
                    Shenandoah Garbage Collector
                </para>
                <para>
                    Használatához a következő argumentumot kell megadni: <command>java -XX:+UseShenanodoahC -jar Something.java</command>
                </para>
            </listitem>
        </orderedlist>
        <para></para>
        <para>
            Igen, lehetséges az OutOfMemoryError kezelése, viszont itt már csak annyit tudunk csinálni, hogy lezárjuk a megnyitott forrásokat és esetleg logoljuk az Error forrását és azt hogy mi volt az utolsó művelet, aminek már nem jutott memória a heap-ben (persze ha ez lehetséges).
        </para>
    </section>
</chapter>

