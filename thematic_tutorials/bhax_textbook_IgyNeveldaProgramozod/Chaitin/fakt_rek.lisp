(defun fakt (szam)
    (if (= szam 0)
        1
        (* szam (fakt (- szam 1)))
    )
)
; (fakt 5)
(format t "A faktoriális eredménye(5): ~D~%" (fakt 5))
