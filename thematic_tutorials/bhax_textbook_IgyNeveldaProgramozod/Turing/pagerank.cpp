#include <stdio.h>
#include <math.h>
#include <iostream>
using namespace std;

void printRanks (double ranks[], int n){
	for(int i = 0; i < n; i++){
        cout << ranks[i] << endl;
    }
}

double distance (double PR[], double PRv[], int n){
    double sum = 0;
	for(int i = 0; i < n; i++){
		sum+= (PRv[i]-PR[i]) * (PRv[i] - PR[i]);
	}
	return sqrt(sum); 
}
void calcPageRank(double linkData[4][4]){
    double PR[4] = {0.0, 0.0, 0.0, 0.0}; 
	double initPR[4] = {1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0};
    for(;;){
        for(int i = 0; i < 4; i++){
            PR[i] = 0;
            for(int j = 0; j < 4; j++){
                PR[i] += linkData[i][j] * initPR[j]; 
            }
        }
        if(distance(PR, initPR, 4) < 0.0000000001){ 
            break;
            }
        for(int i = 0; i < 4;i++){
            initPR[i] = PR[i];
        }
    }
    printRanks(PR, 4);
}
int main (){

	double linkData_normal[4][4] =
	{
        {0.0,     0.0, 1.0/3.0, 0.0},
        {1.0, 1.0/2.0, 1.0/3.0, 1.0},
        {0.0, 1.0/2.0,     0.0, 0.0},
        {0.0,     0.0, 1.0/3.0, 0.0}
	};
    calcPageRank(linkData_normal);
	return 0;
}
