#include <curses.h>
#include <unistd.h>
using namespace std;
int main(){
	WINDOW *console = initscr();

	int x = 0;
	int y = 0;
	int xIncr = 1;
	int yIncr = 1;
	int max_x;
	int max_y;
	for(;;){
		getmaxyx(console, max_y, max_x); //max méret cserélés
		mvprintw(y, x, "0");
		refresh();
		usleep(50000);
		clear();
		x += xIncr*(x/max_x)*-1;
		y += yIncr;
		if(x >= max_x-1)
		{
			xIncr *=-1;
		}
		if(x <= 0)
		{
			xIncr *= -1;
		}
		if(y >= max_y-1)
		{
			yIncr *= -1;
		}
		if(y <= 0)
		{
			yIncr *= -1;
		}
	}
	return 0;
}
