#include <iostream>
using namespace std;

void swapArithmetic(int &a, int &b);
void swapXOR(int &a, int &b);

int main(){
    int a = 23;
    int b = 7;
    cout << "Original a: "<< a << " original b: " << b << endl << endl;

    swapArithmetic(a,b);
    cout << "Arithmetric method:" <<endl;
    cout << "Swapped a: "<< a << " swapped b: " << b << endl << endl;

    a = 23; //restoring original values
    b = 7;
    swapXOR(a,b);
    cout << "XOR method:" << endl;
    cout << "Swapped a: " << a << " swapped b: " << b << endl;
    return 0;
}
void swapArithmetic(int &a, int &b){
    a = a + b; //a += b;
    b = a - b;
    a = a - b; //a -= b;
}
void swapXOR(int &a, int &b){
    //in this case a = 23 = 10111 (binary)
    //in this case b = 7  = 00111 (binary)
    a = a ^ b; //10000
    b = a ^ b; //10111 = original a's value
    a = a ^ b; //00111 = original b's value
}
