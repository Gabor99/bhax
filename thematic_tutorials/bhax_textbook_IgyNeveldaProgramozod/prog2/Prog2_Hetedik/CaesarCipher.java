import java.io.*;
import java.util.Random;

public class CaesarCipher {
    public static String cipher(String message, int offset) {
        StringBuilder result = new StringBuilder();
        for (char character : message.toCharArray()) {
            if (character != ' ') {
                int originalAlphabetPosition = character - 'a';
                int newAlphabetPosition = (originalAlphabetPosition + offset) % 26;
                char newCharacter = (char) ('a' + newAlphabetPosition);
                result.append(newCharacter);
            } else {
                result.append(character);
            }
        }
        return result.toString();
    }
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter writer;
        int offset = 0;
        switch (args.length){
            case 1: {
                FileWriter file = new FileWriter(args[0]);
                writer = new BufferedWriter(file);
                offset = new Random().nextInt(25)+1;
                break;
            }
            case 2:{
                FileWriter file = new FileWriter(args[0]);
                writer = new BufferedWriter(file);
                offset = Integer.parseInt(args[1]);
                break;
            }
            default:{
                writer = new BufferedWriter(new OutputStreamWriter(System.out));
                offset = new Random().nextInt(25)+1;
                break;
            }
        }
        String input = reader.readLine();
        while(input.length() != 0) {
            writer.write(cipher(input, offset));
            writer.newLine();
            input = reader.readLine();
        }
        reader.close();
        writer.close();
    }
}
