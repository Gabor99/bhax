// g++ java_classes.cpp -o java_classes -lboost_system -lboost_filesystem
// ./java_classes

#include <iostream>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/xpressive/regex_actions.hpp>

using namespace std;
int main(){
    vector<string> classes; 
    boost::filesystem::path targetDir("./src/");

    boost::filesystem::recursive_directory_iterator iter(targetDir), eod;

    BOOST_FOREACH(boost::filesystem::path const& i, make_pair(iter, eod))
    {
        if (is_regular_file(i) && i.filename().string().find(".java") != string::npos){
            cout << i.filename().string() << endl;
            classes.push_back(i.filename().string());
        }
    }
    cout << "===========================================" << endl;
    cout << classes.size() << " db java osztály van az src.zip-ben"<< endl;
}
