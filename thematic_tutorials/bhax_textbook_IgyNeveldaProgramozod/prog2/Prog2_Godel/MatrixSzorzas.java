import java.util.Arrays;
import java.util.stream.IntStream;

public class MatrixSzorzas {
    public static void main(String[] args) {
        double[][] elso = { { 3, 7 }, { 0, 1 }, { 2, 5 } };
        double[][] masodik = { { 4, 1 }, { 8, 3 } };

        double[][] result = Arrays.stream(elso).map(r ->
                IntStream.range(0, masodik[0].length).mapToDouble(i ->
                        IntStream.range(0, masodik.length).mapToDouble(j -> r[j] * masodik[j][i]).sum()
                ).toArray()).toArray(double[][]::new);

        System.out.println(Arrays.deepToString(result));
    }
}
