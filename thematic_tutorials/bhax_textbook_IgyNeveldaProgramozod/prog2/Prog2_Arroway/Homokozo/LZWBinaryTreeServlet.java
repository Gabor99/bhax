import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.File;
import java.io.FileWriter;
import java.io.FileInputStream;
import javax.servlet.*;
import javax.servlet.http.*;

public class LZWBinaryTreeServlet extends HttpServlet
{
    public LZWBinaryTreeServlet ()
    {
    	root = new Node('/');
    	currentNode = root;
    }
  
    public void insert (char b)
    {
       
        if (b == '0')
        {
           
            if (currentNode.getLeftChild () == null)	
            {
                
                Node n = new Node ('0');
                
                
                
                currentNode.newLeftChild (n);
                
                currentNode = root;
            }
            else			
            {
                
                currentNode = currentNode.getLeftChild ();
            }
        }
        
        else
        {
            if (currentNode.getRightChild () == null)
            {
                Node n = new Node ('1');
                currentNode.newRightChild (n);
                currentNode = root;
            }
            else
            {
                currentNode = currentNode.getRightChild ();
            }
        }
    }
    
   
    public int getDepth ()
    {
    	 	depth = maxDepth = 0;
    	    recDepth (root);
    	    return maxDepth-1;
    }
    public double getMean ()
    {
    	depth = sumOfMean = numberOfNodes = 0;
        recMean (root);
        mean = ((double) sumOfMean) / numberOfNodes;
        return mean;
    }
    public double getVariance ()
    {
    	mean = getMean ();
        sumOfVar = 0.0;
        depth = numberOfNodes = 0;

        recVar (root);

        if (numberOfNodes - 1 > 0)
            variance = Math.sqrt (sumOfVar / (numberOfNodes - 1));
        else
            variance = Math.sqrt (sumOfVar);

        return variance;
    }

    public void recDepth (Node  n)
    {
        if (n != null)
        {
            ++depth;
            if (depth > maxDepth)
                maxDepth = depth;
            recDepth (n.getRightChild ());
            
            
            recDepth (n.getLeftChild ());
            --depth;
        }
    }


public void recMean (Node  n)
{
    if (n != null)
    {
        ++depth;
        recMean (n.getRightChild ());
        recMean (n.getLeftChild ());
        --depth;
        if (n.getRightChild () == null && n.getLeftChild () == null)
        {
            ++numberOfNodes;
            sumOfMean += depth;
        }
    }
}

public void recVar (Node  n)
{
    if (n != null)
    {
        ++depth;
        recVar (n.getRightChild ());
        recVar (n.getLeftChild ());
        --depth;
        if (n.getRightChild () == null && n.getLeftChild () == null)
        {
            ++numberOfNodes;
            sumOfVar += ((depth - mean) * (depth - mean));
        }
    }
}
    public void printTree (PrintWriter os)
    {
        depth = 0;
        printTree (root, os);
    }

    class Node
    {
      
        Node (char b)
        {
        	if(b == '0' || b == '1')
        		value = b;
        	else
        	value = '/';
        };
 
        Node getLeftChild () 
        {
            return leftChild;
        }
        
        Node getRightChild () 
        {
            return rightChild;
        }
        
        void newLeftChild (Node gy)
        {
            leftChild = gy;
        }
        
        void newRightChild (Node gy)
        {
            rightChild = gy;
        }
      
        char getValue () 
        {
            return value;
        }
       

        
        private char value;
       
        private Node leftChild;
        private Node rightChild;
    };

   
    Node currentNode;
    
    private int depth, sumOfMean, numberOfNodes;
    private double sumOfVar;
    

    
    public void printTree (Node  n, PrintWriter os)
    {
        
        if (n != null)
        {
            ++depth;
            printTree (n.getLeftChild (), os);
            
            
            for (int i = 0; i < depth; ++i)
                os.print("---");
            os.print(n.getValue () + "(" + depth + ")\n");
            printTree (n.getRightChild (), os);
            --depth;
        }
    }
   
	protected Node root;
    protected int maxDepth;
    protected double mean, variance;


    @Override
    public void  doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        String inFile = request.getParameter("text");

        PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter("o")));

        byte[] b = inFile.getBytes();
        LZWBinaryTreeServlet theTree = new LZWBinaryTreeServlet();		
        response.setContentType("text/html");
        
        boolean inComment = false;
        for(int i=0; i<b.length; i++)
        {
            if (b[i] == 0x3e) 
            {			
                inComment = true;
                continue;
            }
            if (b[i] == 0x0a) 
            {			
                inComment = false;
                continue;
            }
            if (inComment)
            {
                continue;
            }
            if (b[i] == 0x4e)
            {
                continue;
            }
            for (int j = 0; j < 8; ++j)
            {
                if ((b[i] & 0x80) != 0)
                {
                    theTree.insert('1');
                } 
                else
                {
                    theTree.insert('0');
                }
                b[i] <<= 1;
            }
        }        
        
        theTree.printTree(output);		

        output.print("depth " + theTree.getDepth () + "\n");
        output.print("mean " + theTree.getMean () + "\n");
        output.print("var " + theTree.getVariance () + "\n");

        output.close();
        PrintWriter out = response.getWriter();

        out.println("<html>");
        out.println("<head><title>LZWBinFa</title></head>");
        out.println("<body>");
        out.println("<h1>Binfa</h1>");
        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("o")))))
        {
            for(String line; (line = br.readLine()) != null; ) 
            {
                out.println("<p>" + line + "</p>");
            }
        }
        out.println("</body></html>");
       

    }

    
};





