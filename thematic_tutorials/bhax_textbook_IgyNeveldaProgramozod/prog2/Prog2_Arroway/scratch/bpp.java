class BPP
{
	
	char[] hexLetters = {'A','B','C','D','E', 'F'};
	BPP(int n)
	{
        double pi = 0.0;
		double pi1 = 4* getS(n,1);
		double pi2 = 2*getS(n,4);
		double pi3 = getS(n,5);
		double pi4 = getS(n,6);
		
		pi = pi1-pi2-pi3-pi4;
		pi = pi-StrictMath.floor(pi);
		
		
		//pi 0 és 1 között
		StringBuilder str = new StringBuilder();
		
		while(pi != 0.0)
		{
			int hex = (int)StrictMath.floor(16.0*pi);
			str.append((hex < 10) ? String.valueOf(hex) : hexLetters[hex-10]);
			
          
			pi = 16.0*pi - StrictMath.floor(16.0*pi);
			
		}
		
		System.out.println(str.toString());
		
	}
	
	private long nMod(int n, int k)
	{
		int t = 1;
		
		while(t <= n)
		{
			t = 2*t;
			
		}
		long r = 1;
		
		while(true)
		{
			if(n >= t)
			{
				r = 16*r % k;
				n = n - t;
			}
			t/=2;
			if(t < 1)
				break;
			if(t>=1)
			{
				r = r*r % k;
			}
		}
		
		return r;
		
	}
	
	public double getS(int d, int j)
	{
		double ret = 0.0;
		
		for(int i=0; i<d; i++)
		{
			ret+= (nMod(d-i, 8*i+j))/(double)(8*i+j);
			
		}
		for(int i=d+1; i<2*d; i++)
		{
			ret+= StrictMath.pow(16, d-i)/(double)(8*i+j);
		}
		
		return ret - StrictMath.floor(ret);
	}
	
	public static void main(String[] args)
	{
		new BPP(1000000);
		
	}
}

