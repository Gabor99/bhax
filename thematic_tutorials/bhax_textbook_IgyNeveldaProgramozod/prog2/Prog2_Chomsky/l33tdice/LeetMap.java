import java.util.TreeMap;

public class LeetMap {
    public static void main(String[] args){
        TreeMap<Character, String[]> leetmap = new TreeMap<>();
        leetmap.put('a', new String[] {"4", "4", "@", "/-\\"});
        leetmap.put('b', new String[] {"b", "8", "|3", "|}"});
        leetmap.put('c', new String[] {"c", "(", "<", "{"});
        leetmap.put('d', new String[] {"d", "|)", "|]", "|}"});
        leetmap.put('e', new String[] {"3", "3", "3", "3"});
        leetmap.put('f', new String[] {"f", "|=", "ph", "|#"});
        leetmap.put('g', new String[] {"g", "6", "[", "[+"});
        leetmap.put('h', new String[] {"h", "4", "|-|", "[-]"});
        leetmap.put('i', new String[] {"1", "1", "|", "!"});
        leetmap.put('j', new String[] {"j", "7", "_|", "_/"});
        leetmap.put('k', new String[] {"k", "|<", "1<", "|{"});
        leetmap.put('l', new String[] {"l", "1", "|", "|_"});
        leetmap.put('m', new String[] {"m", "44", "(V)", "|\\/|"});
        leetmap.put('n', new String[] {"n", "|\\|", "/\\/", "/V"});
        leetmap.put('o', new String[] {"0", "0", "()", "[]"});
        leetmap.put('p', new String[] {"p", "/o", "|D", "|o"});
        leetmap.put('q', new String[] {"q", "9", "O_", "(,)"});
        leetmap.put('r', new String[] {"r", "12", "12", "|2"});
        leetmap.put('s', new String[] {"s", "5", "$", "$"});
        leetmap.put('t', new String[] {"t", "7", "7", "'|'"});
        leetmap.put('u', new String[] {"u", "|_|", "(_)", "[_]"});
        leetmap.put('v', new String[] {"v", "\\/", "\\/", "\\/"});
        leetmap.put('w', new String[] {"w", "VV", "\\/\\/", "(/\\)"});
        leetmap.put('x', new String[] {"x", "%", ")(", ")("});
        leetmap.put('y', new String[] {"y", "y", "y", "y"}); //Ide mit kéne?
        leetmap.put('z', new String[] {"z", "2", "7_", ">_"});

        leetmap.put('0', new String[] {"D", "0", "D", "0"});
        leetmap.put('1', new String[] {"I", "I", "L", "L"});
        leetmap.put('2', new String[] {"Z", "Z", "Z", "e"});
        leetmap.put('3', new String[] {"E", "E", "E", "E"});
        leetmap.put('4', new String[] {"h", "h", "A", "A"});
        leetmap.put('5', new String[] {"S", "S", "S", "S"});
        leetmap.put('6', new String[] {"b", "b", "G", "G"});
        leetmap.put('7', new String[] {"T", "T", "j", "j"});
        leetmap.put('8', new String[] {"X", "X", "X", "X"});
        leetmap.put('9', new String[] {"g", "g", "j", "j"});
    }
}
