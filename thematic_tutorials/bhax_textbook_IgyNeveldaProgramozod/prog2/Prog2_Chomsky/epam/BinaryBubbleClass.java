public class BinaryBubbleClass {
    private int data[];
    private int actualSize = 0;
    public BinaryBubbleClass(int size){
        if (size < 1)
            System.out.println("Érténytelen kollekció méret!");
        else
            data = new int[size];
    }
    public void add(int element) {
        //todo: Több elemet is lehessen hozzáadni!!!
        if(data.length - actualSize != 0)
        {
            data[actualSize] = element;
            actualSize++;
        }
        else {
            System.out.println("Nincs több szabad hely a kollekcióba!");
        }
    }
    public int binarySearch(int kezdo, int veg, int keresett) {
        this.bubbleSort();
        if (veg >= kezdo) {
            int kozepso = kezdo + (veg - kezdo) / 2;
            if (data[kozepso] == keresett)
                return kozepso;
            if (data[kozepso] > keresett)
                return binarySearch(kezdo, kozepso - 1, keresett);
            return binarySearch(kozepso + 1, veg, keresett);
        }
        return -1; //-1 index nincs
    }
    public void bubbleSort(){
        for (int i = 0; i < actualSize - 1; i++) {
            for (int j = 0; j < actualSize - i - 1; j++) {
                if (data[j] > data[j + 1]) {
                    int temp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = temp;
                }
            }
        }
    }
    public int getActualSize(){
        return actualSize;
    }
    public void printValues(){
        for (int x: data) {
            System.out.print(x + " ");
        }
        System.out.print(System.lineSeparator());
    }
    public static void main(String[] args) {
        BinaryBubbleClass test = new BinaryBubbleClass(5);
        test.add(17);
        test.add(4);
        test.add(94);
        test.add(12);
        test.add(24);
        System.out.println("Az eredeti értékek:");
        test.printValues();
        test.bubbleSort();
        System.out.println("A rendezett értékek:");
        test.printValues();
        int keresett_elem = 94;
        int keresett_index = test.binarySearch(0, test.getActualSize(),keresett_elem);
        if(keresett_index != -1){
            System.out.println("A keresett elem " + keresett_elem +" indexe "+ keresett_index);
        }
        else
            System.out.println("Nincs ilyen elem a kollekcióba!");
    }
}
